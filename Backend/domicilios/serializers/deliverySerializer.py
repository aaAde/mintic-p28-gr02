from domicilios.models.account     import Account
from domicilios.models.delivery    import Delivery
from domicilios.models.dish        import Dish  
from rest_framework                import serializers

class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model  = Delivery
        fields = ['menu_selected', 'price', 'register_date', 'note', 'customer_account']

    def to_representation(self, obj):
            account_customer  = Account.objects.get(id=obj.customer_account_id)
            delivery          = Delivery.objects.get(id=obj.id)
            dish_selected     = Dish.objects.get(id=obj.menu_selected_id)
            
            return {
                'id'            : delivery.id,
                'price'         : delivery.price,
                'register_date' : delivery.register_date,
                'note'          : delivery.note,
                'customer_account' : {
                    'id'            : account_customer.id,
                    'balance'       : account_customer.balance,
                    'isActive'      : account_customer.isActive
                },
                'menu_selected' :{
                    'name'         : dish_selected.name,    
                    'ingredient_1' : dish_selected.ingredient_1,
                    'ingredient_2' : dish_selected.ingredient_2,
                    'ingredient_3' : dish_selected.ingredient_3,
                    'isAviable'    : dish_selected.isAviable

                }  
            }
        
             