from domicilios.models.dish import Dish
from rest_framework import serializers

class DishSerializer(serializers.ModelSerializer):
     class Meta:
         model=Dish
         fields=['name','ingredient_1','ingredient_2','ingredient_3','isAviable']

