from django.db import models
from .account  import Account
from .dish import Dish

class Delivery(models.Model):
    id               = models.AutoField(primary_key=True)
    customer_account = models.ForeignKey(Account, related_name='customer', on_delete=models.CASCADE)
    menu_selected    = models.ForeignKey(Dish,related_name='dish',on_delete=models.CASCADE)
    price            = models.IntegerField(default=0)
    register_date    = models.DateTimeField(auto_now_add=True, blank=True)
    note             = models.CharField(max_length=100)