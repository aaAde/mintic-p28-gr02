from django.db import models

class Dish(models.Model):
    id=models.AutoField(primary_key=True)
    name=models.CharField( max_length = 50)
    ingredient_1=models.CharField(max_length = 50)
    ingredient_2=models.CharField(max_length = 50)
    ingredient_3=models.CharField(max_length = 50)
    isAviable=models.BooleanField(default=True)

  