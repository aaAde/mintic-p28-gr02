from django.conf                                      import settings
from rest_framework                                   import generics, status
from rest_framework.response                          import Response
from rest_framework.permissions                       import IsAuthenticated
from rest_framework_simplejwt.backends                import TokenBackend

from domicilios.models.dish                          import Dish
from domicilios.serializers.dishSerializer           import DishSerializer

class DishDetailView(generics.RetrieveAPIView):
    serializer_class   = DishSerializer
    queryset           = Dish.objects.all()
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class DishCreateView(generics.CreateAPIView):
    serializer_class   = DishSerializer
    #permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        serializer = DishSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("Dish was saved", status=status.HTTP_201_CREATED)


class DishUpdateView(generics.UpdateAPIView):
    serializer_class   =  DishSerializer
    #permission_classes = (IsAuthenticated,)
    queryset           = Dish.objects.all()
    def put(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)        
 
class  DishDeleteView(generics.DestroyAPIView):
    serializer_class   =  DishSerializer
    #permission_classes = (IsAuthenticated,)
    queryset           = Dish.objects.all()
    def delete(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


