from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .deliveryView   import DeliveryAccountView, DeliveryDetailView, DeliveryCreateView, DeliveryDeleteView, DeliveryUpdateView
from .dishView       import DishDetailView,DishCreateView,DishUpdateView,DishDeleteView 