from django.contrib     import admin
from .models.user       import User
from .models.account    import Account
from .models.delivery   import Delivery
from .models.dish       import Dish 

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Delivery)
admin.site.register(Dish)