from django.contrib                 import admin
from django.urls                    import path
from domicilios                     import views  
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

urlpatterns = [
    path('admin/',                                      admin.site.urls),  # use defaul Djando Admin
    path('login/',                                      TokenObtainPairView.as_view()), # use credentials to return tokens
    path('refresh/',                                    TokenRefreshView.as_view()), # generate new access token
    path('user/',                                       views.UserCreateView.as_view()), # create a new user
    path('user/<int:pk>/',                              views.UserDetailView.as_view()), # check info for an specific user based on id(pk)
    path('delivery/',                                   views.DeliveryCreateView.as_view()), # create a new transaction
    path('delivery/<int:user>/<int:pk>/',               views.DeliveryDetailView.as_view()), # view information for a transaction
    path('delivery/all/<int:user>/<int:account>/',      views.DeliveryAccountView.as_view()), # view all transactions for an specific account
    path('delivery/update/<int:user>/<int:pk>/',        views.DeliveryUpdateView.as_view()), # update a transaction
    path('delivery/remove/<int:user>/<int:pk>/',        views.DeliveryDeleteView.as_view()), # delete a transaction
    path('menu/<int:pk>/',                              views.DishDetailView.as_view()), # view menu
    path('menu/create/',                                views.DishCreateView.as_view()), # create menu
    path('menu/update/<int:pk>/',                       views.DishUpdateView.as_view()), # create menu
    path('menu/remove/<int:pk>/',                       views.DishDeleteView.as_view()), # create menu
]   