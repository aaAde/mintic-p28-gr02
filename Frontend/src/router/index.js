import { createRouter, createWebHistory } from 'vue-router'
import App from '../App.vue'
import Login from '../components/Login.vue'
import SignUp from '../components/SignUp.vue'
import Home from '../components/Home.vue'
import Account from '../components/Account.vue'
import MenuDish from '../components/MenuDish.vue'
import NewMenu from '../components/NewMenu.vue'

const routes = [
  {
    path: '/',
    name: 'app',
    component: App
  },
  {
    path: '/user/login', 
    name: 'logIn',
    component: Login
  },
  {
    path: '/user/signup',
    name: 'signUp',
    component: SignUp
  },

  {
    path: '/user/home',
    name: 'home',
    component: Home
  },

  {
    path: '/user/account',
    name: 'account',
    component: Account
  },

  {
    path: '/user/menu',
    name: 'menu',
    component: MenuDish
  },

  {
    path: '/user/menu/new',
    name: 'newMenu',
    component: NewMenu
  },

] 

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router 
